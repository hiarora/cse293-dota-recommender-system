#  DOTA Upgrade Sequence Recommender system #

Undertook machine learning research project under Prof. Julian
McAuley. Designed a recommender system for an online popular game DOTA. Using their Steam credentials, a user can get a
strategically suitable upgrade sequence based on their history and history of other players. Used techniques like SVD, SVD++
(Latent Factor models), and Bias models to generate 3 different kinds of sequences.

* Source Code : [link](https://bitbucket.org/hiarora/cse293-dota-recommender-system/src/29b3baa718ffeef6f84fcfc8d11784f131644a35?at=master)
* Project Report : [link](https://drive.google.com/file/d/0B3-_RVSYTjgOV0prb3RaUUY2MWc/view?usp=sharing)
* Recommender : [link](http://ec2-54-183-221-129.us-west-1.compute.amazonaws.com/dotap/)